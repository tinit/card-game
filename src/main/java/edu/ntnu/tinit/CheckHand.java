package edu.ntnu.tinit;

import java.util.ArrayList;
import java.util.List;

public class CheckHand {
    public ArrayList<PlayingCard> hands;

    public CheckHand(ArrayList<PlayingCard> hands) {
        this.hands = hands;
    }

    public ArrayList<PlayingCard> getHands(){
        return hands;
    }

    public int calculateSum() {
        int sum = 0;
        for (PlayingCard card : hands) {
            sum += card.getFace();
        }
        return sum;
    }


    public List<PlayingCard> getHearts() {
        return hands.stream()
                .filter(card -> card.getSuit() == 'H')
                .toList();
    }

    public boolean checkIfHeart() {
        return hands.stream()
                .anyMatch(card -> card.getSuit() == 'H');
    }

    public boolean checkIfQueenOfSpades() {
        return hands.stream()
                .anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);
    }

    public boolean checkIfFlush() {
       boolean Hearts = hands.stream().allMatch(card -> card.getSuit() =='H');
       boolean Clove = hands.stream().allMatch(card -> card.getSuit() =='C');
       boolean Diamond = hands.stream().allMatch(card -> card.getSuit() =='D');
       boolean Spade = hands.stream().allMatch(card -> card.getSuit() =='S');
       if(Hearts){
           return true;
       }
       if(Clove){
           return true;
       }
       if(Diamond)
           return true;

        return Spade;
    }

}
