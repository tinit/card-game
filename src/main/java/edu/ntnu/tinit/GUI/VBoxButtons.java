package edu.ntnu.tinit.GUI;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class VBoxButtons {
    private final VBox vBox;
    Button checkHand = new Button("Check hand");
    Button deal_cards = new Button("Deal cards");

    private final Button seeCard = new Button("See Cards");

    public VBoxButtons(){
        this.vBox = new VBox();
        vBox.setPadding(new Insets(10));

        vBox.setSpacing(10);

        vBox.setStyle("-fx-border-color: #000000");

        vBox.setAlignment(Pos.CENTER);


        checkHand.setPrefSize(100,20);
        deal_cards.setPrefSize(100,20);


        VBox.setVgrow(deal_cards, Priority.ALWAYS);
        VBox.setVgrow(checkHand, Priority.ALWAYS);
        vBox.setPrefWidth(200);
        vBox.setPrefHeight(400);
        vBox.setMinHeight(300);


        vBox.getChildren().addAll(deal_cards,checkHand,seeCard);
    }

    public VBox getVBox(){
        return vBox;
    }

    public Button getDealCards() {
        return deal_cards;
    }

    public Button getCheckHand() {
        return checkHand;
    }

    public Button getSeeCard(){
        return seeCard;
    }
}
