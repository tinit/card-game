package edu.ntnu.tinit.GUI;

import edu.ntnu.tinit.CheckHand;
import edu.ntnu.tinit.DeckOfCards;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class CardGridPane {

    private final GridPane grid;


    Label labelSum = new Label();

    Label labelQueenOfSpades = new Label();

    Label labelHearts = new Label();

    Label labelFlush = new Label();
    private final VBoxButtons vBoxButtons;
    private final DeckOfCards deckOfCards;


    private CheckHand checkHand;

    private int number;

    private boolean cardsDealt = false;




    public CardGridPane(VBoxButtons vBoxButtons){
        this.vBoxButtons = vBoxButtons;
        this.grid = new GridPane();
        this.deckOfCards = new DeckOfCards();
        this.checkHand = new CheckHand(deckOfCards.dealHand(number));
        grid.setHgap(10);
        grid.setVgap(5);
        grid.setPadding(new Insets(0, 10, 0, 10));

        Text sumLabel = new Text("Sum of Face:");
        GridPane.setHgrow(sumLabel, Priority.ALWAYS);
        Text heartsLabel = new Text("Hearts:");
        GridPane.setHgrow(heartsLabel, Priority.ALWAYS);
        Text queenOfSpadesLabel = new Text("Ace of Spades:");
        GridPane.setHgrow(queenOfSpadesLabel, Priority.ALWAYS);
        Text flush = new Text("Flush?:");
        GridPane.setHgrow(flush,Priority.ALWAYS);


        grid.add(sumLabel, 18, 1);
        grid.add(heartsLabel,36,1);
        grid.add(queenOfSpadesLabel,18,4);
        grid.add(flush,36,4);
        grid.add(labelSum,19,1);
        grid.add(labelQueenOfSpades,19,4);
        grid.add(labelHearts,37,1);
        grid.add(labelFlush,37,4);
        Button dealCards = vBoxButtons.getDealCards();
        dealCards.setOnAction(actionEvent ->{
            final Stage dialog = new Stage();
            dialog.setTitle("Choose a number");
            dialog.initModality(Modality.APPLICATION_MODAL);
            GridPane gridPaneDialog = new GridPane();
            gridPaneDialog.setHgap(10);
            gridPaneDialog.setVgap(10);
            gridPaneDialog.setPadding((new Insets(25, 25, 25, 25)));
            TextField textField = new TextField();
            textField.setPrefWidth(100);
            gridPaneDialog.add(textField,2,0);
            Label label1 = new Label("Enter a number");
            gridPaneDialog.add(label1,0,0);
            Button button = new Button("done");
            button.setPrefSize(100,100);
            Label error = new Label();
            gridPaneDialog.add(error, 2,3);
            button.setOnAction(actionEvent1 -> {
                        try{
                            this.number = Integer.parseInt(textField.getText());
                        } catch (NumberFormatException ex){
                            error.setText("Please enter a number");
                        }
                        if(number<=52 && number>0){
                            this.checkHand = new CheckHand(deckOfCards.dealHand(number));
                            cardsDealt=true;
                            dialog.close();
                        } else error.setText("invalid number");
                    });
            gridPaneDialog.add(button,2,2);
            Scene dialogScene = new Scene(gridPaneDialog, 300, 130);
            dialog.setScene(dialogScene);
            dialog.show();
        });

        Button checkHand1 = vBoxButtons.getCheckHand();
        checkHand1.setOnAction(actionEvent -> {
            labelSum.setText(Integer.toString(checkHand.calculateSum()));
            labelQueenOfSpades.setText(Boolean.toString(checkHand.checkIfQueenOfSpades()));
            labelFlush.setText(Boolean.toString(checkHand.checkIfFlush()));
            if (checkHand.getHearts().size() > 0) {
                grid.getChildren().removeIf(node -> GridPane.getRowIndex(node) == 1 && GridPane.getColumnIndex(node) >= 37);
            }
            if(checkHand.getHearts().size()>0){
                for(int i=0; i<checkHand.getHearts().size();i++){
                    Label hearts = new Label(checkHand.getHearts().get(i).getAsString());
                    grid.add(hearts,37+i,1);
                }
            }
            else labelHearts.setText("No hearts");
        });

    }

    public GridPane getGrid(){
        return grid;
    }


    public CheckHand getCheckHand() {
        return checkHand;
    }

    public boolean isCardsDealt() {
        return cardsDealt;
    }


}
