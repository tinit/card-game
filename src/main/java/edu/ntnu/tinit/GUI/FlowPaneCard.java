package edu.ntnu.tinit.GUI;

import edu.ntnu.tinit.CheckHand;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;

public class FlowPaneCard {
    private final FlowPane flowPane;

    private final VBoxButtons vBoxButtons;

    private final CardGridPane cardGridPane;

    private final CheckHand checkHand;


    public FlowPaneCard(VBoxButtons vBoxButtons, CardGridPane cardGridPane) {
        this.flowPane = new FlowPane();
        this.vBoxButtons = vBoxButtons;
        this.cardGridPane = cardGridPane;
        this.checkHand = cardGridPane.getCheckHand();

        flowPane.setStyle("-fx-border-color: black");
        flowPane.setAlignment(Pos.CENTER);
        Button seeCards = vBoxButtons.getSeeCard();

        seeCards.setOnAction(e -> {
            if (cardGridPane.isCardsDealt() && flowPane.getChildren().size()==0) {
                for(int i=0; i<cardGridPane.getCheckHand().getHands().size();i++){
//                    Text text = new Text("/cards/"+cardGridPane.getCheckHand().getHands().get(i).getAsString()+".png");
//                    System.out.println(text.getText());
//                    text.setFont(new Font(20));
//                    flowPane.getChildren().add(text);
                    String imagePath = cardGridPane.getCheckHand().getHands().get(i).getAsString() + ".png";
                    Image image = new Image(imagePath);
                    ImageView imageView = new ImageView(image);
                    imageView.setFitWidth(50);
                    imageView.setPreserveRatio(true);
                    flowPane.getChildren().add(imageView);
                }
            }

            if(cardGridPane.isCardsDealt() && flowPane.getChildren().size()>0){
                flowPane.getChildren().clear();
                for(int i=0; i<cardGridPane.getCheckHand().getHands().size();i++){
//                    Text text = new Text(cardGridPane.getCheckHand().getHands().get(i).getAsString()+
//                            " ");
//                    text.setFont(new Font(20));
//                    flowPane.getChildren().add(text);
                    String imagePath = cardGridPane.getCheckHand().getHands().get(i).getAsString() + ".png";
                    Image image = new Image(imagePath);
                    ImageView imageView = new ImageView(image);
                    imageView.setFitWidth(100);
                    imageView.setPreserveRatio(true);
                    flowPane.getChildren().add(imageView);
                }
            }
        });



    }
    public FlowPane getFlowPane() {
        return flowPane;
    }

}
