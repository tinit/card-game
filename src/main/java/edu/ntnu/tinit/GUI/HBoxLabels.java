package edu.ntnu.tinit.GUI;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;

public class HBoxLabels {
    private final HBox hBox;

    public HBoxLabels(){
        this.hBox = new HBox();
        hBox.setAlignment(Pos.CENTER_LEFT);
        hBox.setPadding(new Insets(20));
        hBox.setSpacing(12);
        hBox.setStyle("-fx-border-color: black");
        hBox.setStyle("-fx-border-radius: 100.0");
        hBox.setPrefHeight(100);
        hBox.setPrefWidth(900);
        hBox.setMaxWidth(900);
        hBox.setMinWidth(400);
    }

    public HBox getHBox(){
        return hBox;
    }

}
