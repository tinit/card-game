package edu.ntnu.tinit.GUI;
import javafx.application.Application;

import javafx.scene.Scene;

import javafx.scene.layout.*;

import javafx.stage.Stage;



public class CardGame extends Application{

    private final VBoxButtons vBoxButtons = new VBoxButtons();

    private final CardGridPane cardGridPane = new CardGridPane(vBoxButtons);
    private final HBoxLabels hBoxLabels = new HBoxLabels();
    private final HBoxTitle hBoxTitle = new HBoxTitle();

    private final FlowPaneCard flowPaneCard = new FlowPaneCard(vBoxButtons, cardGridPane);




    public static void main(String[] args){
    launch(args);
    }

    @Override
    public void start(Stage primaryStage){
        primaryStage.setTitle("CardGame");

        BorderPane border = new BorderPane();

        HBox hBox = hBoxLabels.getHBox();
        VBox vBox = vBoxButtons.getVBox();
        HBox hBox1 = hBoxTitle.getHBox();
        GridPane grid = cardGridPane.getGrid();
        hBox.getChildren().add(grid);


        FlowPane flowPane = flowPaneCard.getFlowPane();

        border.setCenter(flowPane);

        border.setLeft(vBox);
        border.setTop(hBox1);
        border.setBottom(hBox);

        Scene scene = new Scene(border);
        primaryStage.setScene(scene);
        primaryStage.show();
    }



}
