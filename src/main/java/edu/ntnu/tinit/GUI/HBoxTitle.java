package edu.ntnu.tinit.GUI;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;

public class HBoxTitle {
    private final HBox hBox;
    Label label = new Label("Card game");

    public HBoxTitle(){
        this.hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        hBox.setPadding(new Insets(12,12,12,15));
        hBox.setSpacing(12);

        hBox.setStyle("-fx-border-color: black");
        hBox.setStyle("-fx-border-radius: 100.0");


        label.setFont(new Font("Verdana", 20));

        HBox.setHgrow(label, Priority.ALWAYS);

        hBox.getChildren().addAll(label);

        hBox.setPrefHeight(70);
        hBox.setPrefWidth(450);
    }


    public HBox getHBox(){
        return hBox;
    }


}
