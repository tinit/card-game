package edu.ntnu.tinit;

import java.util.ArrayList;
import java.util.Random;

public class DeckOfCards {
    private final ArrayList<PlayingCard> deckOfCards;


    public DeckOfCards() {
        deckOfCards = new ArrayList<>();
        char[] suits = {'S', 'H', 'D', 'C'};
        int[] values = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
        for (char suit : suits) {
            for (int value : values) {
                deckOfCards.add(new PlayingCard(suit, value));
            }
        }
    }

    public ArrayList<PlayingCard> getDeckOfCards() {
        return deckOfCards;
    }

    public ArrayList<PlayingCard> dealHand(int n){
        ArrayList<PlayingCard> hand = new ArrayList<>(n);
        Random random = new Random();
        for(int i=0; i<n;i++){
            int randomIndex = random.nextInt(deckOfCards.size());
            hand.add(deckOfCards.get(randomIndex));
        }
        return hand;
    }

}

